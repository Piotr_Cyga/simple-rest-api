package sda.karyna.karynexshop.dao;

import org.springframework.stereotype.Component;
import sda.karyna.karynexshop.Shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class ShopDAO {
    private HashMap<Integer, Shop> listOfShops;

    public ShopDAO() {
        this.listOfShops = new HashMap<>();
    }

    public HashMap<Integer, Shop> getListOfShops() {
        return listOfShops;
    }

    public void addShop(Shop shop) {
        listOfShops.put(shop.getId(), shop);
    }

    public void removeShop(int shopId) {
        listOfShops.remove(shopId);
    }

    public List<Shop> listShops() {
        List<Shop> list = new ArrayList<>();
        for (Shop shop : listOfShops.values()
                ) {
            list.add(shop);
        }
        return list;
    }

    public Optional<Shop> getShopByOwner(String owner) {
        for (Shop s : listOfShops.values()
                ) {
            if (s.getOwner().equals(owner)) {
                return Optional.ofNullable(s);
            }
        }
        return Optional.empty();
    }

    public Optional<Shop> getShopByName(String name) {
        for (Shop s : listOfShops.values()
                ) {
            if (s.getName().equals(name)) {
                return Optional.ofNullable(s);
            }
        }
        return Optional.empty();
    }
}



