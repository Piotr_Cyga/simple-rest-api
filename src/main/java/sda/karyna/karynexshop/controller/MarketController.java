package sda.karyna.karynexshop.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sda.karyna.karynexshop.Shop;
import sda.karyna.karynexshop.dao.ShopDAO;

import java.util.List;
import java.util.Optional;

@RestController
public class MarketController {

    @Autowired
    private ShopDAO shopDao;


    @RequestMapping(value = "/shop/add", method = RequestMethod.GET)
    public ResponseEntity<String> add(@RequestParam("name") String name, @RequestParam("adress") String adress,
                                  @RequestParam("ownerName") String ownerName) {
        ModelAndView shopView = new ModelAndView();
        Shop shop = new Shop();
        shop.setAddress(adress);
        shop.setName(name);
        shop.setOwner(ownerName);
        shopView.addObject("id", shop);
        shopView.addObject("ownerName", ownerName);
        shopView.addObject("name", name);
        shopView.addObject("adress", adress);
        shopDao.addShop(shop);
        return ResponseEntity.ok("OK!");
    }

    @RequestMapping(value = "/shop/remove", method = RequestMethod.DELETE)
    public ResponseEntity removeShop(@RequestParam("id") Integer id) {
        shopDao.removeShop(id);
        return ResponseEntity.ok("ok!");
    }

    @RequestMapping(value = "/shop/list", method = RequestMethod.GET)
    public ResponseEntity<List<Shop>> listOfShops() {
        return ResponseEntity.ok(shopDao.listShops());
    }

    @RequestMapping(value = "/shop/addshop", method = RequestMethod.POST)
    public ResponseEntity<String> addShop(@RequestBody Shop shop){
        shopDao.addShop(shop);
        return ResponseEntity.ok("Shop added!");
    }
    @RequestMapping(value = "/shop/searchbyname", method = RequestMethod.POST)
    public ResponseEntity<Shop> searchByName(@RequestBody String name){
        Optional<Shop> shop = shopDao.getShopByName(name);
        if(shop.isPresent()){
            Shop result = shop.get();
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.notFound().build();
    }
    @RequestMapping(value = "/shop/searchbyowner", method = RequestMethod.POST)
    public ResponseEntity<Shop> searchByOwner(@RequestBody String name){
        Optional<Shop> shop = shopDao.getShopByOwner(name);
        if(shop.isPresent()){
            Shop result = shop.get();
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.notFound().build();
    }

}
